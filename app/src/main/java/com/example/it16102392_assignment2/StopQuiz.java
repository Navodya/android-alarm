package com.example.it16102392_assignment2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class StopQuiz extends AppCompatActivity implements View.OnClickListener {

    Button one,two,three,four;
    TextView question;

    String answer;
    Random random;
    Questions q= new Questions();
    private int questionLength= q.qutns.length;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stop_quiz);

        random=new Random();

        one = findViewById(R.id.answer1);
        one.setOnClickListener(this);

        two = findViewById(R.id.answer2);
        two.setOnClickListener(this);

        three = findViewById(R.id.answer3);
        three.setOnClickListener(this);

        four = findViewById(R.id.answer4);
        four.setOnClickListener(this);

        question=findViewById(R.id.question);
        NextQuestion(random.nextInt(questionLength));
    }

    @Override
    public void onClick(View v) {
        AlarmBroadcaster ab = new AlarmBroadcaster();

        switch (v.getId()) {
            case R.id.answer1:
                if (one.getText() == answer) {
                    ab.OffAlarm();
                    Toast.makeText(StopQuiz.this, "Alarm is switched off ", Toast.LENGTH_LONG).show();
                    gotoMain();
                }
                else
                {
                    NextQuestion(random.nextInt(questionLength));
                }
                break;

            case R.id.answer2:
                if (two.getText() == answer) {
                    ab.OffAlarm();
                    Toast.makeText(StopQuiz.this, "Alarm is switched off ", Toast.LENGTH_LONG).show();
                    gotoMain();
                }
                else
                {
                    NextQuestion(random.nextInt(questionLength));
                }
                break;

            case R.id.answer3:
                if (three.getText() == answer) {
                    ab.OffAlarm();
                    Toast.makeText(StopQuiz.this, "Alarm is switched off ", Toast.LENGTH_LONG).show();
                    gotoMain();
                }
                else
                {
                    NextQuestion(random.nextInt(questionLength));
                }
                break;

            case R.id.answer4:
                if (four.getText() == answer) {
                    ab.OffAlarm();
                    Toast.makeText(StopQuiz.this, "Alarm is switched off ", Toast.LENGTH_LONG).show();
                    gotoMain();
                }
                else
                {
                    NextQuestion(random.nextInt(questionLength));
                }
                break;
        }
    }
        private void NextQuestion(int num){
            question.setText(q.getQuestion(num));
            one.setText(q.getChoice1(num));
            two.setText(q.getChoice2(num));
            three.setText(q.getChoice3(num));
            four.setText(q.getChoice4(num));

            answer = q.getCorrectAnswer(num);
        }

        public void gotoMain() {
            Intent intent = new Intent(StopQuiz.this, MainActivity.class);
            startActivity(intent);
        }

}
