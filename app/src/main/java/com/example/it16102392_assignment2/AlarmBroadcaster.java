package com.example.it16102392_assignment2;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class AlarmBroadcaster extends Service{

    DatabaseHelper DBHelper;
    String presentTime;
    private static String hours;
    private static String minutes;
    private Timer Alarmtimer;
    private TimerTask timerTask;
    static int x=0;
    Calendar currentTime;
    static int count = 0;
    private static MediaPlayer mediaPlayer;
    //Uri uri;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
         super.onStartCommand(intent, flags, startId);

         DBHelper=new DatabaseHelper(getApplicationContext());
        //start the programme
        startTimer();

         return START_STICKY;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if(DBHelper.getAllAlarms1().getCount()>0){
            Intent addIntent = new Intent(this,BroadcastService.class);
            sendBroadcast(addIntent);
        }
    }

    public void startTimer(){

        Alarmtimer = new Timer();

        //Initialize Timer
        setTimer();

        Alarmtimer.schedule(timerTask,1000,1000);

    }

    public void setTimer(){
        x++;
        timerTask= new TimerTask() {
            @Override
            public void run() {

                String arrayAlarm[]=DBHelper.getAllData();

                if (arrayAlarm.length > 0)
                {
                            ringAlarm(arrayAlarm[0].substring(8,13));
                            System.out.println("oooooo data: " + arrayAlarm[0]);
                           // System.out.println("oooooo data " + arrayAlarm[1]);
                           // System.out.println("oooooo data " + arrayAlarm[2]);
                }
            }


        };
    }

    public  void ringAlarm(String ringTime){

        currentTime=Calendar.getInstance();
        hours=Integer.toString(currentTime.get(Calendar.HOUR_OF_DAY));
        minutes=Integer.toString(currentTime.get(Calendar.MINUTE));

        if(currentTime.get(Calendar.HOUR_OF_DAY) < 10)  hours = "0" + currentTime.get(Calendar.HOUR_OF_DAY);
        if(currentTime.get(Calendar.MINUTE) < 10)  minutes = "0" + currentTime.get(Calendar.MINUTE);

        presentTime= hours + ":" + minutes;
        System.out.println("oooooo data current: " + ringTime + "count : " + count);
        //Uri uriAlarm= Uri.parse("content://media/internal/audio/media/60");
        Uri uri=Uri.parse("EXTERNAL_CONTENT_URI");
        Cursor cursor = DBHelper.getAllAlarms1();
        if(cursor.moveToFirst()){
            uri=Uri.parse(cursor.getString(2));

        }
        System.out.println("oooooo data eQQQ: " + ringTime.equalsIgnoreCase(presentTime));
        if(ringTime.equalsIgnoreCase(presentTime) && count==0){
            System.out.println("oooooo data ffffffffffffff :" );
            mediaPlayer=MediaPlayer.create(this,uri);
            mediaPlayer.setLooping(true);
            mediaPlayer.start();
            count++;
            createNotificationChannel();
        }
    }
    public void OffAlarm(){
        DBHelper.deleteAlarm();
        mediaPlayer.stop();
        count=0;
    }

    public void createNotificationChannel() {
        int NOTIFICATION_ID = 234;

        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        String CHANNEL_ID = "my_channel_01";
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            CharSequence name = "my_channel";
            String Description = "New channel";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            notificationChannel.setDescription(Description);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{400, 500, 400, 300, 200, 400});
            notificationChannel.setShowBadge(false);

            notificationManager.createNotificationChannel(notificationChannel);
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.alarm1)
                .setContentTitle("Alarm!!!!")
                .setContentText("It`s Ringing")
                .setOngoing(true)
                .setSound(null);

        Intent resultIntent = new Intent(this, StopQuiz.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(resultPendingIntent);

        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }
}
