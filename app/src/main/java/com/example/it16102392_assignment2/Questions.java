package com.example.it16102392_assignment2;

public class Questions {

    public String qutns[]={
            "What is the symbol of water",
            "What is 25 x 5",
            "If Kamal is twice the age of Nimal and Nimal is 4.How old is Kamal in 2 years",
            "Who is the Villain in Avengers Infinity war"
    };

    public String answers[][]={
        {"H2O","O2H","H2S","H2"},
        {"200","250","225","125"},
        {"4","10","8","6"},
            {"Thanos","Loki","Vulture","Bucky"}

    };

    public String correctAnswr[]={
            "H2O","125","10","Thanos"
    };

    public String getQuestion(int a){
        String qes=qutns[a];
        return qes;
    }

    public String getCorrectAnswer(int a) {
        String answer = correctAnswr[a];
        return answer;
    }
    public String getChoice1(int a){
        String choice =answers[a][0];
        return choice;
    }
    public String getChoice2(int a){
        String choice = answers[a][1];
        return choice;
    }

    public String getChoice3(int a){
        String choice = answers[a][2];
        return choice;
    }

    public String getChoice4(int a){
        String choice = answers[a][3];
        return choice;
    }
}
