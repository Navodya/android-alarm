package com.example.it16102392_assignment2;

import android.content.Intent;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;

import java.util.HashMap;

public class SetAlarm extends AppCompatActivity {

    private TimePicker simpleTimePicker;
    private Button buttonSetAlarm;
    private Spinner spinner;
    private EditText alarmName;
    DatabaseHelper dbHelper;
    private static MediaPlayer player;
    Uri alarmUri;

    private static HashMap<String, Uri> ringTones = new HashMap<String, Uri>();
    private static String selectedRingtoneUri = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_alarm);

        widgetInitialize();

        listRingtones();

        dbHelper = new DatabaseHelper(this);

        buttonOnClickListners();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                selectedRingtoneUri = ringTones.get(spinner.getSelectedItem()).toString();
                alarmUri = ringTones.get(spinner.getSelectedItem());
                player = MediaPlayer.create(SetAlarm.this, alarmUri);
                player.start();

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                                player.stop();
                    }
                }, 2000);
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }
    public void widgetInitialize() {
        simpleTimePicker = (TimePicker)findViewById(R.id.timePicker); // initiate a time picker
        simpleTimePicker.setIs24HourView(true); // set 24 hours mode for the time picker

        buttonSetAlarm = findViewById(R.id.setAlarmButton);

        spinner = (Spinner)findViewById(R.id.spinner);
        spinner.setPrompt("Select Alarm Tone");

        alarmName = findViewById(R.id.inputText);

    }


    public void listRingtones() {
        RingtoneManager manager = new RingtoneManager(this);
        manager.setType(RingtoneManager.TYPE_RINGTONE);
        Cursor cursor = manager.getCursor();
        String[] ringToneTiltles = new String[cursor.getCount()];

        int id = 0;

        while (cursor.moveToNext()) {
            String title = cursor.getString(RingtoneManager.TITLE_COLUMN_INDEX);
            Uri ringtoneURI = manager.getRingtoneUri(cursor.getPosition());

            ringTones.put(title, ringtoneURI);
            ringToneTiltles[id] = title;

            id +=1;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(SetAlarm.this,
                android.R.layout.simple_spinner_item,ringToneTiltles);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        System.out.println("ooooo "+adapter);
    }


    public void buttonOnClickListners() {
        buttonSetAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String hours = Integer.toString(simpleTimePicker.getCurrentHour());
                String minutes = Integer.toString(simpleTimePicker.getCurrentMinute());

                if(simpleTimePicker.getCurrentHour() < 10)  hours = "0" + simpleTimePicker.getCurrentHour();
                if(simpleTimePicker.getCurrentMinute() < 10)  minutes = "0" + simpleTimePicker.getCurrentMinute();

                String time = hours + ":" +minutes;

                dbHelper.addData(time,alarmName.getText().toString(), selectedRingtoneUri);
                stopService(new Intent(SetAlarm.this, AlarmBroadcaster.class));
                finish();
            }
        });
    }
}
