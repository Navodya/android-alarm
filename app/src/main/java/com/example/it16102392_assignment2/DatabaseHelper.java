package com.example.it16102392_assignment2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.HashMap;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_TABLE_NAME="AlarmTable";
    private static final String COL1="time";
    private static final String COL2="title";
    private static final String COL3="ringingTone";

    public DatabaseHelper(Context context){
        super(context.getApplicationContext(),DB_TABLE_NAME,null,1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String createTable = "CREATE TABLE " + DB_TABLE_NAME + " (" +
                COL1 + " text primary key," +
                COL2 + " text," +
                COL3 + " text )";

        db.execSQL(createTable);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void addData(String dateTime,String alarmTitle,String tone){

        ContentValues values= new ContentValues();
        values.put(COL1,dateTime);
        values.put(COL2,alarmTitle);
        values.put(COL3,tone);

        SQLiteDatabase db = this.getWritableDatabase();
        long result=db.insert(DB_TABLE_NAME, null, values);

        System.out.println("ddddddd "+dateTime + " = " + alarmTitle + " = " + tone);
        System.out.println("ddddddd x "+result);
        System.out.println("ddddddd1 "+COL1 + " = " + COL2 + " = " + COL3);
        db.close();

    }
@NonNull
    public String[] getAllData(){
        int id=0;
        String selectQ="SELECT * FROM "+DB_TABLE_NAME+
                " ORDER BY" + " "+COL1 ;
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery(selectQ,null);

        String alarmA[]=new String[cursor.getCount()];


        HashMap<String,String> alarms = new HashMap<String,String>();

        if(cursor.moveToFirst()){
            String string;

            do{
                alarms.put(cursor.getString(0),cursor.getString(1));
                string="Time is "+cursor.getString(0)+" \n  "+cursor.getString(1);

                alarmA[id]=string;
                id++;
                System.out.println("xxxxxxxxxxxxxxxx "+string);
            }while(cursor.moveToNext());
        }


        return alarmA;
    }
    public Cursor getAllAlarms1() {

        String selectQuery = "SELECT * FROM " + DB_TABLE_NAME
                + " ORDER BY " + COL1 ;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        return cursor;
    }


    public void deleteAlarm(){
        SQLiteDatabase db=getReadableDatabase();
        String select=COL1+" LIKE ?";
        String[] alarms=this.getAllData();
        String abc=alarms[0];
        String[] values = {abc.substring(0,5)};
        System.out.println("v"+abc);
        db.delete(DB_TABLE_NAME,select,values);

    }

    public int DeleteSelectedAlarm(String id){
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(DB_TABLE_NAME, COL1 + " = ?", new String[] { id });

    }

}
