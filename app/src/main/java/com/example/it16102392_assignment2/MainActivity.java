package com.example.it16102392_assignment2;

import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    DatabaseHelper DBHelper;
    private static ListView listView;
    Button deleteButton;
    private static String[] arrayAlarm;
    static Intent serviceIntent;
    private ArrayAdapter arrayAdapter;
    private AlarmBroadcaster alarmBroadcaster;
    private Timer timer;
    private TimerTask task;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SetAlarm.class);
                MainActivity.this.startActivity(intent);
            }
        });
        alarmBroadcaster=new AlarmBroadcaster();
        serviceIntent = new Intent(MainActivity.this,alarmBroadcaster.getClass());
        if (!isMyServiceRunning(AlarmBroadcaster.class)) {
            startService(serviceIntent);
        }

        listView = (ListView) findViewById(R.id.AlarmList);
        DBHelper= new DatabaseHelper(this);
        arrayAlarm = DBHelper.getAllData();

        viewDataList();


        if(arrayAlarm.length > 0) {
            System.out.println("vvvv"+arrayAlarm.length);
            listView.setAdapter(arrayAdapter);
        }

        ViewAlarmdetails();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean isAlarmServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {

                return true;
            }
        }
        Log.i ("isMyServiceRunning?", false+"");
        return false;
    }

    @Override
    protected void onDestroy() {
        stopService(serviceIntent);
        Log.i("MAINACT", "onDestroy!");
        super.onDestroy();
    }

    public boolean isMyServiceRunning(Class<?> serviceClass) {

        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        Log.i("MAINACT", "onDestroy! " + manager.getRunningServices(Integer.MAX_VALUE));
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {

            if (serviceClass.getName().equals(service.service.getClassName())) {

                return true;
            }
        }
        return false;
    }

    public void viewDataList() {
        arrayAlarm = DBHelper.getAllData();
        arrayAdapter = new ArrayAdapter<>(this, R.layout.listview, R.id.titleAlarm, arrayAlarm);
        listView.setAdapter(arrayAdapter);

//        if (arrayAlarm.length == 1) {
//            Log.i("alarms 0", arrayAlarm[0] + "");
//        }
//
//        else if (arrayAlarm.length == 2) {
//            Log.i("alarms 1", arrayAlarm[0] + "");
//            Log.i("alarms 1", arrayAlarm[1] + "");
//        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("xxxxxxxxxxxxxxxxxxx");
        viewDataList();
    }

    public void ViewAlarmdetails(){

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DialogAlertAlarm(position);
            }
        });
    }

    public void DialogAlertAlarm(int position){

        Constant.SelectedTime= arrayAlarm[position].substring(8,13);
        //System.out.println("xxxxxx "+vv);
        AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
        dialog.setTitle("Delete Alarm");
        dialog.setMessage( "Do you want to delete alarm ?");

        dialog.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //DBHelper.deleteAlarm();
//                arrayAlarm = DBHelper.getAllData();
//                viewDataList();
                DeleteAlarm();


            }
        });
        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        dialog.show();

    }

    public void DeleteAlarm(){
        int result = DBHelper.DeleteSelectedAlarm(Constant.SelectedTime);

        System.out.println("xxxxdeletexxxxx " + Constant.SelectedTime + " vvv ");
        if (result > 0) {
            arrayAlarm = DBHelper.getAllData();
            viewDataList();
            Toast.makeText(MainActivity.this, "Successfully Deleted", Toast.LENGTH_LONG).show();
            Intent intent_nav = new Intent(MainActivity.this, MainActivity.class);
            startActivity(intent_nav);
        } else {
            Toast.makeText(MainActivity.this, "Sorry, It went something Wrong!", Toast.LENGTH_LONG).show();
        }

    }
}

